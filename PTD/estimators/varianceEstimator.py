
from transformers.table import Table
from meanEstimator import Moyenne
from abstractEstimator import AbstractEstimator


class Variance(AbstractEstimator):
    """pour le calculm de variance et d'écart type"""

    def __init__(self, var: str):
        self.var = var

    def fit(self, table: Table):
        total = 0
        moyenne = Moyenne(self.var)
        ind = (table.header).index(self.var)
        l = 0
        print("0")
        m = ((moyenne.fit(table)).body)[0][0]
        for i in table.body:
            print("1")
            total += (m - i[ind])**2
            print("2")
            l += 1
        return Table([self.var], [[total/len(table.body)]])


if __name__ == '__main__':
    import doctest
    doctest.testmod()
