
from transformers.table import Table
from abstractEstimator import AbstractEstimator

class Moyenne(AbstractEstimator):
    """calcule la moyenne"""
    def __init__(self,var:str):
        self.var=var
        """constructeur de moyenne"""
      

    def fit(self,table:Table):
        total = 0
        ind=(table.header).index(self.var)
        j=0
        for i in table.body:
            total = total + i[ind]
            j+=1
        result=Table([self.var],[[total/j]])
        return result


if __name__ == '__main__':
    import doctest
    doctest.testmod()