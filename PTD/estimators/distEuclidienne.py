
from math import sqrt

class DistEuclidienne:
    def __init__(self,point1:list,point2:list):
        self.point1 = point1
        self.point2 = point2

    def distEuclidienne(self):
        '''deux points ayant le même nombre de coordonnées'''
        distance = 0
        for i in range(0,len(self.point1)):
            distance += ((self.point1)[i]-(self.point2)[i])**2
        return sqrt(distance)

