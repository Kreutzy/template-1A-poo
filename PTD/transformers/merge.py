''' Ce module contient une seule classe
'''
from table import Table
from abstractTransformers import AbstractTransformers
class Merge(AbstractTransformers) :
    ''' Classe qui effectue une jointure entre
    deux tables.
    La jointure se fait suivant une variable qui
    doit être présente dans les deux tables
    
    Attributes :
    ------------
    
    __tableToMerge : Table
    Une des tables intervenant dans la jointure
    
    __var : str
    La variable sur laquelle s'effectue la jointure
    '''
    def __init__(self,table:Table,var:str) :
        self.__tableToMerge=table
        self.__var=var
        '''Constructeur de la classe
        
        Parameters :
        ------------
        
        __tableToMerge : Table
        Une des tables de la jointure
        
        __var : str
        La variable de jointure
        
        Examples :
        ----------
        >>>a=Table([],[])
        >>>b=Merge(a,"age")
        '''
        

    def transform(self,tab2:Table) :
        '''
        

        Parameters
        ----------
        tab2 : Table
            La deuxième table intervenant dans
            la jointure

        Raises
        ------
        Exception
            Une erreur se produit quand la variable
            est absente d'une des deux tables'

        Returns
        -------
        result : TYPE
            La table correspondant à la jointure
            

        '''
        if not((self.__var in (self.__tableToMerge).header) and 
        (self.__var in tab2.header)) :
            raise Exception("Problème : variable absente dans une des tables")
        else :
            b=(tab2.header).copy()
            ind=b.index(self.__var)
            del(b[ind])
            result=Table(((self.__tableToMerge).header).copy()+b.copy(),[])
            ctab2=tab2.copy()
            ind=(ctab2.header).index(self.__var)
            ind1=((self.__tableToMerge).header).index(self.__var)
            for i in (self.__tableToMerge).body :
                for j in (ctab2.getSubTable(self.__var,[i[ind1]])).body :
                    del(j[ind])
                    k=(i+j).copy()
                    (result.body).append(k)
            return result





def Union(lst1:list, lst2:list):
    final_list = list(set(lst1) | set(lst2))
    return final_list

