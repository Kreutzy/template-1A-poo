from abstractImporter import AbstractImporter
from table import Table
from csv import reader

class Csvimporter(AbstractImporter) :
    
    def __init__(self,file : str, encoding : str) :
        self.__file=file
        self.__encoding=encoding
        
    def transform(self, tab : Table) :
        folder='C:\\Users\\Alvarenga\\Documents\\'
        data = []
        with open(folder + self.__file, encoding=self.__encoding) as csvfile :
            covidreader = reader(csvfile, delimiter=';');
            for row in covidreader :
                data.append(row)
        
