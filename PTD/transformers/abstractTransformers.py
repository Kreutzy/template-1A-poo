from table import Table
from abstractOperation import AbstractOperation, abstractmethod

class AbstractTransformers(AbstractOperation) :

    
    @abstractmethod    
    def transform(self,tab:Table) :
        pass
    
    def execute(self,tab:Table) :
        return self.transform(tab)
        

     
        
        