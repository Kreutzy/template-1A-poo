from datetime import date
from table import Table
from abstractTransformers import AbstractTransformers
from estimators.meanEstimator import Moyenne

class CenterTransformers(AbstractTransformers) :

    def __init__(self,var : str) :
        self.__var=var

    def transform(self,tab:Table) :

        result=Table([],[])
        if not(self.__var in tab.header) :
            raise Exception("La variable cherchée n'est pas présente!")
        else :
            m=Moyenne(self.__var)
            c=m.fit(tab)
            ind=(tab.header).index(self.__var)
            for i in tab.body :
                s=i.copy()   
                s[ind]=s[ind]-(c.body)[0][0]
                (result.body).append(s)

        result.header=(tab.header).copy()
        return result