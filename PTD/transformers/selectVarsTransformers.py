
from datetime import date
from table import Table
from abstractTransformers import AbstractTransformers

class SelectVarsTransformers(AbstractTransformers) :

    def __init__(self,dateBegin : date,dateEnd :date) :
        self.__dataBegin=dateBegin
        self.__dateEnd=dateEnd

    def transform(self,tab:Table) :

        result=Table([],[])
        if not("date" in tab.header) :
            raise Exception("une variable de type date n'est pas présente!")
        else :
            ind=(tab.header).index("date")
            for i in tab.body :
                if ((i[ind] >= self.__dateBegin) and 
                (i[ind] <= self.__dateEnd)) :
                    (result.body).append(i.copy())

        result.header=(tab.header).copy()
        return result