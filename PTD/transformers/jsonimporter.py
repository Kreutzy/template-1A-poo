from abstractImporter import AbstractImporter
from table import Table
from json import load

class Jsonimporter(AbstractImporter) :
    
    def __init__(self,file : str) :
        self.__file=file
        
    def impor(self, tab : Table) :
        folder='C:\\Users\\Alvarenga\\Documents\\'
        data = []
        with open(folder + self.__file, "r") as read_file :
            data=load(read_file)
        return data

