from abc import ABC, abstractmethod
from table import Table
from abstractTransformers import AbstractTransformers

class AbstractImporter(AbstractTransformers) :
        
        
        @abstractmethod
        def impor(self, tab : Table) :
            pass
        
        def transform(self, tab : Table) :
            return self.impor(tab)