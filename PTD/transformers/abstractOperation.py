from abc import ABC, abstractmethod
from table import Table

class AbstractOperation(ABC) :

    @abstractmethod
    def execute(self,tab:Table) :
        pass
        