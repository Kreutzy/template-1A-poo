'''
C'est un module contenant une seule classe
'''
from table import Table
from abstractTransformers import AbstractTransformers

class FilterVarsTransformers(AbstractTransformers) :
    '''
    Cette classe a pour objectif de modéliser
    une sélection. Ne sont retenues dans la table initiale
    que les enregistrements d'une certaine liste 
    de variables'
    
    Attributes :
    ------------
    
    __vars : list
    La liste de variables à conserver
    
    __type : str
    définit si ob garde les variables ("keep")
    ou si on les élimine ("drop")
    
    Examples :
    ----------
    >>>a= FilterVarsTransformers(["age","poids"])
    '''
    def __init__(self, vars:list,type:str="keep") :
        self.__vars=vars
        self.__type=type
        ''' Constructeur de la classe
        
        Parameters :
        ------------
        
        __vars : list
        La liste de variables
        
        __type : str
        Le type de sélection
        
        '''

    def transform(self, tab:Table):
        '''
        C'est la méthode qui transforme une table
        en entrée pour faire la sélection'

        Parameters
        ----------
        tab : Table
            La table en entrée sur laquelle
            on fait la sélection

        Returns
        -------
        result : TYPE
            La table ensortie avec les variables
            que l'on garde ou sans les variables
            que l'on élimine
            
        Examples :
        ----------
        >>>a=Table(["age","poids"],[[20,70]])
        >>>b=FilterVarsTransformers("age")
        >>>c=b.transform(a)
        >>>c.body
        [[20]]
        '''

        
        result=tab.copy()
        if (self.__type=="keep") :
            for i in tab.header :
                if not(i in self.__vars) :
                    print("remove")
                    k= (result.header).index(i)
                    result.removeColumn(k)
        elif (self.__type=="drop") :
            for i in tab.header :
                if i in self.__vars :
                    k=(result.header).index(i)
                    result.removeColumn(k)
        return result
        
if __name__ == '__main__':
    import doctest
    doctest.testmod()