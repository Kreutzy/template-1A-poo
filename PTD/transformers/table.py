'''Ce module contient une seule classe
'''
class Table :
    '''Cette classe définit une Table comme une
    structure comportant un header (liste de noms
    de variables) et un body (liste de listes)
    comportant les enregistrements.
    '''
    def __init__(self,header:list,body:list) :
        self.header=header
        self.body=body
        '''Constructeur de la classe
        
        parameters :
        ------------
        
        header : list
        liste des noms de variables
        
        body : list
        Liste des enregistrements (valeurs)
        
        '''

    def addLine(self,line:list,pos:int) :
        '''
        Cette méthode ajoute une ligne à la table
        à une certaine position

        Parameters
        ----------
        line : list
            La lgne à ajouter
        pos : int
            la position d'insertion dans le
            body.'

        Returns
        -------
        La même table avec la ligne insérée
        
        Examples :
        ----------
        >>> a=Table(["age"],[[10]])
        
        >>> a.addLine([20],0)
        
        >>> a.body
        [[20],[10]]

        '''
        (self.body).insert(pos,line)
        
    
    def getColumn(self,name:str) :
        '''
        Cette méthode retourne la colonne
        correspondant à un nom de variable

        Parameters
        ----------
        name : str
            Le nom de variable de la colonne

        Returns
        -------
        result : list
            Une liste correspondant à la colonne
            cherchée
            
        Examples :
        ----------
        >>> a=Table(["age"],[[20]])
        >>> b=a.getColumn("age")
        >>> b
        [20]

        '''
        result=[]
        pos=(self.header).index(name)
        for i in self.body :
            result.append(i[pos])
        return result

    def removeLine(self,pos:int) :
        '''Cette méthode élimine une ligne de 
        la table
        
        Parameters :
        ------------
        
        pos : int
        La position de la ligne à éliminer
        
        Returns :
        ---------
        La même table où la 
        ligne a été supprimée
        
        Examples :
        ----------
        >>> a=Table(["age"],[[10]])
        >>> a.removeLine(0)
        >>> a.body
        []
        '''
        
        del(self.body[pos])

    def addColumn(self,column:list,name:str,pos:int) :
        '''Cette méthode ajoute une colonne pour
        une certaine variable et position
        
        Parameters :
        ------------
        
        column : list
        La colonne à ajouter
        
        name : str
        Le nom de la variable concernée
        
        pos : int
        La position d'insertion.
        
        Returns :
        ---------
        Result : Table
        La même table après insertion
        
        Examples :
        ----------
        >>> a=Table(["age"],[[10],[20]])
        >>> a.addColumn(["Jean","Jacques"],"prénom",1)
        >>> a.body
        [[10,"jean"],[20,"Jacques"]]
        '''
        (self.header).insert(pos,name)
        i=0
        if self.body==[] :
            (self.body).append(column)
        else :
            for l in self.body :
                l.insert(pos,column[i])
                i+=1

    def removeColumn(self,pos:int) :
        '''
        Cette méthode élimine une colonne à
        une certaine position

        Parameters
        ----------
        pos : int
            La position de l'élimination'

        Returns
        -------
        La même table sans la colonne
        
        Examples :
        ----------
        >>> a=Table(["age"],[[10],[20]])
        >>> a.removeColumn(0)
        >>> a.body
        [[],[]]

        '''
        del(self.header[pos])
        for i in range(0,len(self.body)) :
            z=(self.body[i])
            del(z[pos])

    def copy(self) :
        '''Effectue une copie profonde de la table
        
        
        Examples :
        ----------
        >>> a=Table(["age"],[[10],[20]])
        >>> b=a.copy()
        >>> b.body=[]
        >>> a.body
        [[10],[20]]
        '''
        
        l=[]
        for i in range(0,len(self.body)) :
            a=list.copy(self.body[i])
            l.append(a)
        return Table(list.copy(self.header),l)
    def getSubTable(self,var:str,val:list) :
        '''
        Cette méthode retourne la sous-table
        pour laquelle les valeurs d'une variable
        sont dans une liste'

        Parameters
        ----------
        var : str
            la variable de sélection
        val : list
            la liste des variables

        Raises
        ------
        Exception
            Erreur si la variable est non trouvée
            

        Returns
        -------
        result : TYPE
            La sous-table dont les valeurs de la
            variable sont dans la liste

        '''
        if not(var in self.header) :
            raise Exception("variable non trouvée dans la table")
        else :
            result=Table((self.header).copy(),[])
            ind=(self.header).index(var)
            for i in self.body :
                if i[ind] in val :
                    (result.body).append(i.copy())
            return result

if __name__ == '__main__':
    import doctest
    doctest.testmod()  
