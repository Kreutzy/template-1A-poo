# -*- coding: utf-8 -*-
"""
Created on Sat May 15 14:49:31 2021

@author: Alvarenga
"""
from transformers.abstractOperation import AbstractOperation
from transformers.abstractTransformers import AbstractTransformers
from transformers.aggregationTransformers import AggregationTransformers
from transformers.filterVarsTransformers import FilterVarsTransformers
from transformers.merge import Merge
from transformers.movingAverageTransformers import MovingAverageTransformers
from transformers.pipeline import Pipeline
from transformers.selectVarsTransformers import SelectVarsTransformers
from transformers.table import Table
from transformers.selectValTransformers import SelectValTransformers
from datetime import date
from csv import reader
from transformers.jsonimporter import Jsonimporter

'''
folder ='C:\\Users\\Alvarenga\\Documents\\'
filename ='covid-hospit-incid-reg-2021-03-03-17h20.csv'
data = []
with open(folder + filename, encoding='ISO-8859-1') as csvfile :
    covidreader = reader(csvfile, delimiter=';');
    for row in covidreader :
        data.append(row)
'''
'''
u=Table(["age","poids"],[[10, 60],[20, 80]])
v=Table(["poids","taille"],[[60, 130]])
w=Table(["taille","score"],[[130,2],[140,5]])
print(u.body)
b=Pipeline([["Normal","age"]],u)
c=b.run()
print(c.body)
'''
js=Jsonimporter("VacancesScolaires.json")
d=js.impor([])

