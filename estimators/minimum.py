 
 
import estimators

class minimum:
    def __init__(self,table:Table):
        self.Table = Table
        '''renvoie la valeur minimum partmis les éléments d'une liste
        parameters
        ______
        table : Table
            liste dont on souhaite déterminer le minimum
        
        Return
        ------
        int
            la valeur du minimum de la liste table'''
    def minimum(self,table :Table):
        min = table[0]
        for i in range(1,len(table)):
             if min > table[i]:
                 min = table[i]
        return min
