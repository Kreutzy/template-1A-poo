from transformers.abstractOperation import AbstractOperation, abstractmethod
from transformers.table import Table

class AbstractEstimator(AbstractOperation) :
    '''
    def __init__(self) :
        pass
    '''
    
    @abstractmethod    
    def fit(self,tab : Table) :
        pass
    
    def execute(self,tab:Table) :
        self.fit(tab)