from table import Table
from filterVarsTransformers import FilterVarsTransformers
from abstractTransformers import AbstractTransformers

class AggregationTransformers(AbstractTransformers) :
    ''' Cette classe effectue une agrégation sur une
    table.
    Elle prend une table en entrée et un nom de variable et elle crée
    une nouvelle table avec un header réduit à ce
    nom de variable et comme body la somme des
    valeurs associées à la variable dans la table
    initiale.
    
    attributes :
    ------------
    __var : str
    C'est la variable sur laquelle s'effectue l'agrégation
    examples :
    ----------
    >>> a=AggregationTransformers("age")
    '''
    def __init__(self,var :str) :
        self.__var=var
        '''
        Constructeur de la classe
        
        Parameters :
        ------------
        var : str
        Le nom de la variable sur laquelle se fait
        l'agrégation
        '''

    def transform(self,tab:Table) :
        '''
        C'est la méthode qui prend en entrée
        une table et donne en sortie la table
        contenant l'agrégation.
        

        Parameters
        ----------
        tab : Table
            C'est la table sur laquelle se fait
            l'agrégation'

        Raises
        ------
        Exception
            Indique une erreur si var ne fait
            pas partie du header de la table
            en entrée

        Returns
        -------
        result : Table
            Retourne en sortie une table avec
            un header réduit à la variable et un
            body contenant la somme calculée
            
        Examples :
        ----------
        >>> g=AggregationTransformers("age")
        >>> u=Table(["age"],[[10],[20]])
        >>> g.transform(u).body
        [[30]]

        '''
        print("transformAgg")
        if not(self.__var in tab.header) :
            raise Exception("Variable non présente")
        else :
            result=Table([],[])
            somme=0
            m=FilterVarsTransformers([self.__var])
            u=m.transform(tab)
            for i in range(0,len(u.body)) :
                somme+=(u.body)[i][0]
            print("somme="+str(somme))
            print(self.__var)
            result.addColumn([somme],self.__var,0)
            return result