from datetime import date
from table import Table
from abstractTransformers import AbstractTransformers

class SelectValTransformers(AbstractTransformers) :

    def __init__(self,var : str,val : list) :
        self.__var=var
        self.__val=val

    def transform(self,tab:Table) :

        result=Table([],[])
        if not(self.__var in tab.header) :
            raise Exception("La variable cherchée n'est pas présente!")
        else :
            ind=(tab.header).index(self.__var)
            for i in tab.body :
                if (i[ind] in self.__val) :
                    (result.body).append(i.copy())

        result.header=(tab.header).copy()
        return result