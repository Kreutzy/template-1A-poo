'''Ce module contient une seule classe
'''
from table import Table
from abstractTransformers import AbstractTransformers

class MovingAverageTransformers(AbstractTransformers) :
    '''Cette classe effectue
    une moyenne glissante pour une certaine variable
    
    
    
    Attributes:
    -----------
    
    __periode : int
    La demie-largeur de la fenêtre
    
    __var : str
    La variable sur laquelle faire la sélection.
    
    '''
    def __init__(self,periode:int,var:str) :
        self.__periode=periode
        self.__var=var
        '''Constructeur de la classe
        
        Parameters :
        ------------
        
        __periode : int
        La demie-largeur de la fenêtre
        
        __var : str
        Le nom de la variable
        
        Examples :
        ----------
        >>>a=MovingAverageTransformers(2,"age")
        
        '''

    def transform(self,tab:Table) :
        '''
        méthode qui prend en entrée une table
        et redonne la table avec la moyenne glissante
        

        Parameters
        ----------
        tab : Table
            La table en entrée

        Returns
        -------
        La table en sortie qui est la table en entrée
        dans laquelle la variable est associée
        à la moyenne glissante
        
        Examples :
        ----------
        >>>a=Table(["age"],[[10],[20],[30]])
        >>>b=MovingAverageTransformers(2,"age")
        >>>c=b.transform(a)
        >>>c.body
        [[20]]
        

        '''
        tab2=tab.copy()
        c=tab2.getColumn(self.__var)
        ind=(tab2.header).index(self.__var)
        d=[0]*len(c)
        if self.__periode >= (len(tab2.body)-1) :
            for i in range(0,len(c)) :
                d[i]=avg(c,0,len(c)-1)
        else :
            i=0
            while (i< self.__periode) :
                d[i]=avg(c,0,i+self.__periode)
                i+=1
            while (i<=(len(c)-1-self.__periode)) :
                d[i]=avg(c,i-self.__periode,i+self.__periode)
                i+=1
            while (i<=len(c)-1):
                d[i]=avg(c,i-self.__periode,len(c)-1)
                i+=1
        tab2.removeColumn(ind)
        tab2.addColumn(d,self.__var,ind)
        return tab2
        
def avg(l:list,a:int,b:int) :
    s=0
    for i in range(a,b+1) :
        s=s+l[i]
    return s/(b-a+1)

            

