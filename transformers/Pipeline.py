'''Ce module contient une seule classe
'''
from abstractOperation import AbstractOperation
from aggregationTransformers import AggregationTransformers
from filterVarsTransformers import FilterVarsTransformers
from movingAverageTransformers import MovingAverageTransformers
from selectVarsTransformers import SelectVarsTransformers
from merge import Merge
from estimators.kmeansEstimator import Kmeans
from estimators.meanEstimator import Moyenne
from estimators.varianceEstimator import Variance
from table import Table
from selectValTransformers import SelectValTransformers
from centerTransformers import CenterTransformers
from normalTransformers import NormalTransformers

class Pipeline :
    '''Cette classe effectue un pipeline, i.e
    une liste d'opération oùune table en sortie
    devient l'entrée de la prochaine opération
    
    Attributes :
    ------------
    
    operations : list
    La liste des noms d'opérations avec leurs
    paramètres.
    
    tableinput : Table
    La table en entrée du pipeline
    
    '''

    def __init__(self,operations : list,tableinput : Table) :
        self.operations=operations
        self.tableinput=tableinput
        '''Constructeur de la classe
        
        Parameters :
            
        operations : list
        La liste d'opérations avec leurs paramètres
        
        tableinput : table
        La table en entrée du pipeline
        '''

    def run(self) :
        '''
        Méthode qui liste les opérations et effectue
        le pipeline

        Raises
        ------
        Exception
            Une erreur intervient si la
            liste d'opérations est vide.'

        Returns
        -------
        tableoutput : Table
            Retourne la dernière table en sortie.
            
        Examples :
        ----------
        >>>a=Table(["age"],[[10]])
        >>>b=Pipeline([["Aggregation","age"]],a)
        >>>c=b.run()
        >>>c.body
        [[10]]

        '''
        if (self.operations==[]) :
            raise Exception("Liste d'opérations vide")
        else :
            tableinput=(self.tableinput)
            for i in self.operations :
                print(i)
                op=self.traduce(i)
                tableoutput=op.execute(tableinput)
                print("output")
                print(tableoutput.header)
                print(tableoutput.body)
                tableinput=tableoutput
            return tableoutput
        
    def traduce(self,var : list) :
        '''
        Méthode qui traduit un nom d'opération
        en un objet'

        Parameters
        ----------
        var : list
            Liste de noms d'opérations avec
            paramètres'

        Raises
        ------
        Exception
            Une erreur survient si l'opération
            n'est pas reconnue.'

        Returns
        -------
        result : AbstractOperation
            Une opération correspondant au
            nom.
            
        Examples :
        ----------
        >>>b=Pipeline([["Aggregation","age"]],a)
        >>>c=b.traduce(b.operations[0])
        >>>c.__var
        "age"

        '''
        if (var[0]=="Aggregation") :
            result=AggregationTransformers(var[1])
        elif (var[0]=="Filter") :
            result=FilterVarsTransformers(var[1])
        elif (var[0]=="Moving") :
            result=MovingAverageTransformers(var[1],var[2])
        elif (var[0]=="Select") :
            result=SelectVarsTransformers(var[1],var[2])
        elif (var[0]=="Merge") :
            result=Merge(var[1],var[2])
        elif (var[0]=="Kmeans") :
            result=Kmeans(var[1])
        elif (var[0]=="Mean") :
            result=Moyenne(var[1])
        elif (var[0]=="Variance") :
            result=Variance(var[1])
        elif (var[0]=="SelectVal") :
            result=SelectValTransformers(var[1],var[2])
        elif (var[0]=="Center") :
            result=CenterTransformers(var[1])
        elif (var[0]=="Normal") :
            result=NormalTransformers(var[1])
        else :
            raise Exception("Opération non reconnue")
        return result
